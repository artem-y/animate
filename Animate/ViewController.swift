//
//  ViewController.swift
//  Animate
//
//  Created by Artem Yelizarov on 23.02.2020.
//  Copyright © 2020 Artem Yelizarov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - @IBOutlets

    @IBOutlet var trashButton: UIButton!
    @IBOutlet var folderButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var listButton: UIButton!
    
    // MARK: - Private Properties

    private var trashButtonCenter: CGPoint!
    private var folderButtonCenter: CGPoint!
    private var sendButtonCenter: CGPoint!
    
    private var isShowingButtons = true
}

// MARK: - @IBActions

extension ViewController {
    @IBAction func listButtonPressed(_ sender: Any) {
        if isShowingButtons {
            hideButtons()
        } else {
            showButtons()
        }
    }
}

// MARK: - Lifecycle

extension ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        makeButtonsRound()
        recordCenters()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideButtons()
    }
}

// MARK: - Private Methods

extension ViewController {
    private func hideButtons() {
        UIView.animate(withDuration: Default.animationDuration, animations: { [unowned self] in
            self.trashButton.center = self.listButton.center
            self.folderButton.center = self.listButton.center
            self.sendButton.center = self.listButton.center
        }) { [unowned self]
            (complete) in
            if complete {
                self.isShowingButtons = false
            }
        }
    }
    
    private func showButtons() {
        UIView.animate(withDuration: Default.animationDuration, animations: { [unowned self] in
            self.trashButton.center = self.trashButtonCenter
            self.folderButton.center = self.folderButtonCenter
            self.sendButton.center = self.sendButtonCenter
        }) { [unowned self] (complete) in
            if complete {
                self.isShowingButtons = true
            }
        }
    }
    
    private func recordCenters() {
        trashButtonCenter = trashButton.center
        folderButtonCenter = folderButton.center
        sendButtonCenter = sendButton.center
    }

    private func makeButtonsRound() {
        [trashButton, folderButton, sendButton, listButton].forEach {
            $0.layer.cornerRadius = $0.frame.width / 2
        }
    }
}

// MARK: - Default Values

extension ViewController {
    struct Default {
        static let animationDuration = 0.9
    }
}
